package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.DVD;
import sender.Producer;

/**
 * Servlet implementation class DVDServlet
 */
public class DVDServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Producer producer = new Producer();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DVDServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doGet(request, response);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		try {
			String title = request.getParameter("title");
			String year = request.getParameter("year");
			String price = request.getParameter("price");
			
			DVD dvd = new DVD(title, Integer.parseInt(year), Double.parseDouble(price));
			
			producer.sendMessage(dvd.toString());

			System.out.println(dvd.toString());
			out.println("<script type=\"text/javascript\">");
			out.println("alert('DVD introdus cu succes');");
			out.println("</script>");
		} catch (NumberFormatException e) {
			response.sendError(404, "Verificati ca datele introduse sa fie unele valide.");
		}
	}

}
