package receiver;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ReceiveLogs {

	private static final String EXCHANGE_NAME = "logs";
	
	private static List<String> mails = Arrays.asList("maria.bihoi@gmail.com", "todescu.mircea@gmail.com");

	private static int j =  0;
	
	public static void main(String[] argv) throws Exception {
		//se creeaza o conexiune la server
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost"); //conexiune la un broker de pe masina locala
		Connection connection = factory.newConnection();
		final Channel channel = connection.createChannel();

		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

		//lasam server-ul sa dea nume cozile, dupa ce nu mai sunt folosite sunt sterse automat
		String queueName = channel.queueDeclare().getQueue();
		String queueName2 = channel.queueDeclare().getQueue();

		//aici se specifica ca exchange-ul creat sa trimita mesajele la cozi
		channel.queueBind(queueName, EXCHANGE_NAME, "");
		channel.queueBind(queueName2, EXCHANGE_NAME, "");

		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		//pentru a scoate mesajele ce sunt aduse de server
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				try{
					String message = new String(body, "UTF-8");
					
					MailService mailService = new MailService("maria.bihoi@gmail.com","*********");
					System.out.println("Sending mail "+ message);
					for(int i=0;i<mails.size();i++) {
						mailService.sendMail(mails.get(i),"New DVD on stock",message);
					}
					System.out.println("Mail sent");
					//System.out.println(" [x]consumer1 Received '" + message + "'");
				} finally{
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}
		};
		channel.basicConsume(queueName, false, consumer);

		Consumer consumer2 = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				try {
					String message = new String(body, "UTF-8");
					String[] s = message.split(",");
					String name = s[0].substring(11);
					TextFileService fileService = new TextFileService();
					System.out.println("Creating file: "+ message);
					fileService.createFile("file_"+name+".txt", message);
					j++;
					//System.out.println(" [x]consumer2 Received2 '" + message + "'");
				} finally {
					//se anunta ca mesajul a fost procesat
					//false-daca nu exista mesaje multiple
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}
		};
		//acknowledgementul ca a primit mesajul si l-a procesat trebuie sa fie la inceput setat pe fals
		channel.basicConsume(queueName2, false, consumer2);
	}
}
