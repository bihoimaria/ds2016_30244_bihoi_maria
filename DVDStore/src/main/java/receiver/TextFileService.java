package receiver;

import java.io.PrintWriter;

public class TextFileService {

	public TextFileService() {}
	
	public void createFile(String fileName, String content) {
		try{
		    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		    writer.println(content);
		    writer.close();
		} catch (Exception e) {
			System.out.println("Error text file service");
		   e.printStackTrace();
		}
	}
}
