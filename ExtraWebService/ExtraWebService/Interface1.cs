﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtraWebService
{
    interface Interface1
    {
        List<Package> getAllPackages();

        string getUserName(int id);

        int getUserId(string name);
    }
}
