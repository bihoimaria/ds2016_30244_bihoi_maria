﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraWebService
{
    public class Package
    {
        public Package() { }
        public Package(int idSender, int idReceiver, string name, string description, string senderCity,
                string destinationCity, string tracking)
        {
            this.idSender = idSender;
            this.idReceiver = idReceiver;
            this.name = name;
            this.description = description;
            this.senderCity = senderCity;
            this.destinationCity = destinationCity;
            this.tracking = tracking;
        }

        public int id { get; set; }

        public int idSender { get; set; }

        public int idReceiver { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public string senderCity { get; set; }

        public string destinationCity { get; set; }

        public string tracking { get; set; }
    }
}