﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraWebService
{
    public class PackageDAO
    {
        DBConnection db = new DBConnection();
        DBConnection dbase = new DBConnection();

        public PackageDAO() { }

        public List<Package> getAllPackages()
        {
            string query = "select * from package";
            List<Package> packages = new List<Package>();
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();
                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Package input = new Package(Convert.ToInt32(dataReader["sender_iduser"]), Convert.ToInt32(dataReader["receiver_iduser"]),
                        Convert.ToString(dataReader["name"]), Convert.ToString(dataReader["description"]),
                        Convert.ToString(dataReader["senderCity"]), Convert.ToString(dataReader["destinationCity"]),
                        Convert.ToString(dataReader["tracking"]));
                    input.id = Convert.ToInt32(dataReader["idpackage"]);
                    Console.WriteLine(input.ToString());
                    packages.Add(input);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return packages;
            }
            else
            {
                return packages;
            }
        }
    }
}