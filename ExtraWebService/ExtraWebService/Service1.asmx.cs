﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ExtraWebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri2.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService, Interface1
    {

        UserDAO u = new UserDAO();
	    PackageDAO p = new PackageDAO();

        [WebMethod]
        public List<Package> getAllPackages()
        {
            return p.getAllPackages();
        }

        [WebMethod]
        public String getUserName(int id)
        {
            return u.getUserName(id);
        }

        [WebMethod]
        public int getUserId(string name)
        {
            return u.getUserId(name);
        }
    }
}