﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraWebService
{
    public class User
    {
        public User() { }
        public User(string name, string password, string role)
        {
            this.name = name;
            this.password = password;
            this.role = role;
        }

        public int id { get; set; }

        public string name { get; set; }

        public string password { get; set; }

        public string role { get; set; }
    }
}