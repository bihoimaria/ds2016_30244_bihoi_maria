﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraWebService
{
    public class UserDAO
    {
        DBConnection db = new DBConnection();

        public UserDAO() { }

        public int getUserId(string name)
        {
            string query = "select iduser from user where name= '" + name + "'";
            int id = 0;
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                id = (int)cmd.ExecuteScalar();

                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return id;
            }
            else
            {
                return id;
            }
        }

        public string getUserName(int id)
        {
            string query = "select name from user where iduser= " + id;
            string name = "";
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                name = (string)cmd.ExecuteScalar();

                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return name;
            }
            else
            {
                return name;
            }
        }
    }
}