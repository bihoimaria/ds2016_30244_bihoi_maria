package business;

public interface PackageService {

	public boolean addPackage(String sender, String receiver, String name, String description, String senderCity,
			String destinationCity);
	
	public boolean deletePackage(int id);
	
	public boolean registerForTracking(String city, int idPackage);
	
	public boolean updateStatus(String city, int idPackage);
}