package business;

import dao.PackageDAO;
import dao.RouteDAO;
import dao.UserDAO;

public class PackageServiceImpl implements PackageService{
	
	UserDAO u = new UserDAO();
	PackageDAO p = new PackageDAO();
	RouteDAO r = new RouteDAO();

	public boolean addPackage(String sender, String receiver, String name, String description, String senderCity,
			String destinationCity) {
		int idSender = u.getUser(sender);
		int idReceiver = u.getUser(receiver);
		boolean ok = p.addPackage(idSender, idReceiver, name, description, senderCity, destinationCity, "false");
		return ok;
	}
	
	public boolean deletePackage(int id) {
		boolean okP = p.deletePackage(id);
		r.deleteRoute(id);
		return okP;
	}
	
	public boolean registerForTracking(String city, int idPackage){
		p.updateTracking(idPackage, "true");
		boolean ok = r.addRoute(city,idPackage);
		return ok;
	}
	
	public boolean updateStatus(String city, int idPackage){
		boolean ok = r.addRoute(city,idPackage);
		return ok;
	}
}
