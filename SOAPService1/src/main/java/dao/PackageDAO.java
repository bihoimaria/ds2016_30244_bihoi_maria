package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class PackageDAO {

	private DBConnection db = new DBConnection();

	public PackageDAO() {

	}

	public boolean addPackage(int sender, int receiver, String name, String description, String senderCity,
			String destinationCity, String tracking) {
		PreparedStatement pstmt = null;
		boolean ok;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "insert into package (sender_iduser, receiver_iduser, name, description, senderCity, destinationCity, tracking) "
					+ "values (" + sender + ", " + receiver + ", " + "'" + name + "'" + ", " + "'" + description + "'" + ", "
					+ "'" + senderCity + "'" + ", " + "'" + destinationCity + "'" + ", " + "'" + tracking + "')";
			pstmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.executeUpdate();
			db.getConnection().commit();
			pstmt.close();
			System.out.println("ok add package");
			ok = true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail add package");
			ok = false;
			e.printStackTrace();
		}
		return ok;
	}

	public boolean deletePackage(int id) {
		PreparedStatement pstmt = null;
		boolean ok;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "delete from package where idpackage =" + id;
			pstmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.executeUpdate();
			db.getConnection().commit();
			pstmt.close();
			System.out.println("ok delete package");
			ok=true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail delete package");
			ok=false;
			e.printStackTrace();
		}
		return ok;
	}
	
	public boolean updateTracking(int idPackage, String tracking) {
		PreparedStatement pstmt = null;
		boolean ok;
		try {	
			db.getConnection().setAutoCommit(false);
 			String sql = "update package set tracking = '" + tracking + "' where idpackage = " +  idPackage;
 			pstmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
 			pstmt.executeUpdate();
 			db.getConnection().commit();
 			pstmt.close();
 			System.out.println("ok update tracking");
 			ok = true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail update tracking");
			ok = false;
			e.printStackTrace();
		}
		return ok;
	}
}
