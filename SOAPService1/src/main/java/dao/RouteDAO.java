package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class RouteDAO {

	private DBConnection db = new DBConnection();

	public RouteDAO() {

	}

	public boolean addRoute(String city, int idPackage) {
		PreparedStatement pstmt = null;
		boolean ok;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "insert into route (city, package_idpackage) values " + "(" + "'" + city + "'" + ", "  + idPackage + ")";
			pstmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.executeUpdate();
			db.getConnection().commit();
			pstmt.close();
			System.out.println("ok add route");
			ok = true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail add route");
			ok = false;
			e.printStackTrace();
		}
		return ok;
	}
	
	public boolean deleteRoute(int idPackage) {
		PreparedStatement pstmt = null;
		boolean ok;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "delete from route where package_idpackage =" + idPackage;
			pstmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.executeUpdate();
			db.getConnection().commit();
			pstmt.close();
			System.out.println("ok delete route");
			ok=true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail delete route");
			ok=false;
			e.printStackTrace();
		}
		return ok;
	}
}
