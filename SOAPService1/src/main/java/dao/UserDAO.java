package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;

import entities.User;

public class UserDAO {

	private DBConnection db = new DBConnection();

	public UserDAO() {

	}

	public boolean addUser(String name, String pass, String role) {
		PreparedStatement pstmt = null;
		boolean ok;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "insert into user (name, password, role) values (" + "'" + name + "'" + ", " + "'" + pass + "'"
					+ ", " + "'" + role + "')";
			pstmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.executeUpdate();
			db.getConnection().commit();
			pstmt.close();
			System.out.println("ok add user");
			ok = true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail add user");
			ok=false;
			e.printStackTrace();
		}
		return ok;
	}

	public TreeMap<Integer, User> getUsers() {
		TreeMap<Integer, User> users = new TreeMap<Integer, User>();
		Statement stmt = null;
		try {
			db.getConnection().setAutoCommit(false);
			stmt = db.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("select * from user");
			db.getConnection().commit();
			while (rs.next()) {
				User input = new User(rs.getString(2), rs.getString(3), rs.getString(4));
				input.setId(rs.getInt(1));
				System.out.println(input.toString());
				users.put(input.getId(), input);
			}
			stmt.close();
		} catch (SQLException se) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		}
		return users;
	}

	public int getUser(String name) {
		Statement stmt = null;
		int idUser = 0;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "select iduser from user where name= '" + name + "'";
			stmt = db.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			db.getConnection().commit();
			rs.next();
			idUser = rs.getInt(1);
			stmt.close();
			System.out.println("ok dao cu:" + idUser);
			System.out.println("ok get user");
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail get user");
			e.printStackTrace();
		}
		return idUser;
	}
}
