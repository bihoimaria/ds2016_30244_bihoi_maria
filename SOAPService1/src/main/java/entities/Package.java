package entities;

import java.io.Serializable;

public class Package implements Serializable{

	private static final long serialVersionUID = -8381748613371509670L;
	
	private int id;
	private int idSender;
	private int idReceiver;
	private String name;
	private String description;
	private String senderCity;
	private String destinationCity;
	private String tracking;
	
	public Package(){}
	public Package(int idSender, int idReceiver, String name, String description, String senderCity,
			String destinationCity, String tracking) {
		this.idSender = idSender;
		this.idReceiver = idReceiver;
		this.name = name;
		this.description = description;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.tracking = tracking;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSender() {
		return idSender;
	}
	public void setSender(int sender) {
		this.idSender = sender;
	}
	public int getReceiver() {
		return idReceiver;
	}
	public void setReceiver(int receiver) {
		this.idReceiver = receiver;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public String isTracking() {
		return tracking;
	}
	public void setTracking(String tracking) {
		this.tracking = tracking;
	}
	@Override
	public String toString() {
		return "Package [sender=" + idSender + ", receiver=" + idReceiver + ", name=" + name + ", description="
				+ description + ", senderCity=" + senderCity + ", destinationCity=" + destinationCity + ", tracking="
				+ tracking + "]";
	}
}
