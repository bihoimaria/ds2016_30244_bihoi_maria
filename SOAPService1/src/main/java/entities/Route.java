package entities;

import java.io.Serializable;
import java.util.Date;

public class Route implements Serializable{

	private static final long serialVersionUID = 9056120752412671911L;
	
	private int id;
	private String city;
	private Date time;
	private int idPackage;
	
	public Route() {}
	public Route(String city, Date time, int idPackage) {
		this.city = city;
		this.time = time;
		this.idPackage = idPackage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	public int getIdPackage() {
		return idPackage;
	}
	
	public void setIdPackage(int idPackage) {
		this.idPackage = idPackage;
	}
	@Override
	public String toString() {
		return "Route [id=" + id + ", city=" + city + ", time=" + time + ", idPackage=" + idPackage + "]";
	}
}
