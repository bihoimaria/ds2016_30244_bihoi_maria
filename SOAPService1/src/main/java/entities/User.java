package entities;

import java.io.Serializable;

public class User implements Serializable{

	private static final long serialVersionUID = -5741697243344156258L;
	
	private int id;
	private String name;
	private String password;
	private String role;
	
	public User() {}
	public User(String name, String password, String role) {
		this.name=name;
		this.password=password;
		this.role=role;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", role=" + role + "]";
	}
}
