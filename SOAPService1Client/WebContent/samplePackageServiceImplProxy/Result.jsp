<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="samplePackageServiceImplProxyid" scope="session" class="business.PackageServiceImplProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
samplePackageServiceImplProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = samplePackageServiceImplProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        samplePackageServiceImplProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        business.PackageServiceImpl getPackageServiceImpl10mtemp = samplePackageServiceImplProxyid.getPackageServiceImpl();
if(getPackageServiceImpl10mtemp == null){
%>
<%=getPackageServiceImpl10mtemp %>
<%
}else{
        if(getPackageServiceImpl10mtemp!= null){
        String tempreturnp11 = getPackageServiceImpl10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String id_1id=  request.getParameter("id16");
        int id_1idTemp  = Integer.parseInt(id_1id);
        boolean deletePackage13mtemp = samplePackageServiceImplProxyid.deletePackage(id_1idTemp);
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(deletePackage13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
break;
case 18:
        gotMethod = true;
        String city_2id=  request.getParameter("city21");
            java.lang.String city_2idTemp = null;
        if(!city_2id.equals("")){
         city_2idTemp  = city_2id;
        }
        String idPackage_3id=  request.getParameter("idPackage23");
        int idPackage_3idTemp  = Integer.parseInt(idPackage_3id);
        boolean updateStatus18mtemp = samplePackageServiceImplProxyid.updateStatus(city_2idTemp,idPackage_3idTemp);
        String tempResultreturnp19 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(updateStatus18mtemp));
        %>
        <%= tempResultreturnp19 %>
        <%
break;
case 25:
        gotMethod = true;
        String sender_4id=  request.getParameter("sender28");
            java.lang.String sender_4idTemp = null;
        if(!sender_4id.equals("")){
         sender_4idTemp  = sender_4id;
        }
        String receiver_5id=  request.getParameter("receiver30");
            java.lang.String receiver_5idTemp = null;
        if(!receiver_5id.equals("")){
         receiver_5idTemp  = receiver_5id;
        }
        String name_6id=  request.getParameter("name32");
            java.lang.String name_6idTemp = null;
        if(!name_6id.equals("")){
         name_6idTemp  = name_6id;
        }
        String description_7id=  request.getParameter("description34");
            java.lang.String description_7idTemp = null;
        if(!description_7id.equals("")){
         description_7idTemp  = description_7id;
        }
        String senderCity_8id=  request.getParameter("senderCity36");
            java.lang.String senderCity_8idTemp = null;
        if(!senderCity_8id.equals("")){
         senderCity_8idTemp  = senderCity_8id;
        }
        String destinationCity_9id=  request.getParameter("destinationCity38");
            java.lang.String destinationCity_9idTemp = null;
        if(!destinationCity_9id.equals("")){
         destinationCity_9idTemp  = destinationCity_9id;
        }
        boolean addPackage25mtemp = samplePackageServiceImplProxyid.addPackage(sender_4idTemp,receiver_5idTemp,name_6idTemp,description_7idTemp,senderCity_8idTemp,destinationCity_9idTemp);
        String tempResultreturnp26 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(addPackage25mtemp));
        %>
        <%= tempResultreturnp26 %>
        <%
break;
case 40:
        gotMethod = true;
        String city_10id=  request.getParameter("city43");
            java.lang.String city_10idTemp = null;
        if(!city_10id.equals("")){
         city_10idTemp  = city_10id;
        }
        String idPackage_11id=  request.getParameter("idPackage45");
        int idPackage_11idTemp  = Integer.parseInt(idPackage_11id);
        boolean registerForTracking40mtemp = samplePackageServiceImplProxyid.registerForTracking(city_10idTemp,idPackage_11idTemp);
        String tempResultreturnp41 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(registerForTracking40mtemp));
        %>
        <%= tempResultreturnp41 %>
        <%
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>