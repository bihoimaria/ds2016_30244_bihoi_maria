package business;

public class PackageServiceImplProxy implements business.PackageServiceImpl {
  private String _endpoint = null;
  private business.PackageServiceImpl packageServiceImpl = null;
  
  public PackageServiceImplProxy() {
    _initPackageServiceImplProxy();
  }
  
  public PackageServiceImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initPackageServiceImplProxy();
  }
  
  private void _initPackageServiceImplProxy() {
    try {
      packageServiceImpl = (new business.PackageServiceImplServiceLocator()).getPackageServiceImpl();
      if (packageServiceImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)packageServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)packageServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (packageServiceImpl != null)
      ((javax.xml.rpc.Stub)packageServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public business.PackageServiceImpl getPackageServiceImpl() {
    if (packageServiceImpl == null)
      _initPackageServiceImplProxy();
    return packageServiceImpl;
  }
  
  public boolean deletePackage(int id) throws java.rmi.RemoteException{
    if (packageServiceImpl == null)
      _initPackageServiceImplProxy();
    return packageServiceImpl.deletePackage(id);
  }
  
  public boolean updateStatus(java.lang.String city, int idPackage) throws java.rmi.RemoteException{
    if (packageServiceImpl == null)
      _initPackageServiceImplProxy();
    return packageServiceImpl.updateStatus(city, idPackage);
  }
  
  public boolean addPackage(java.lang.String sender, java.lang.String receiver, java.lang.String name, java.lang.String description, java.lang.String senderCity, java.lang.String destinationCity) throws java.rmi.RemoteException{
    if (packageServiceImpl == null)
      _initPackageServiceImplProxy();
    return packageServiceImpl.addPackage(sender, receiver, name, description, senderCity, destinationCity);
  }
  
  public boolean registerForTracking(java.lang.String city, int idPackage) throws java.rmi.RemoteException{
    if (packageServiceImpl == null)
      _initPackageServiceImplProxy();
    return packageServiceImpl.registerForTracking(city, idPackage);
  }
  
  
}