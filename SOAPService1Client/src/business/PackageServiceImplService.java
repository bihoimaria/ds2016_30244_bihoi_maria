/**
 * PackageServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package business;

public interface PackageServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getPackageServiceImplAddress();

    public business.PackageServiceImpl getPackageServiceImpl() throws javax.xml.rpc.ServiceException;

    public business.PackageServiceImpl getPackageServiceImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
