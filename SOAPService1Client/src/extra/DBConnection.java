package extra;

import java.sql.*;

public class DBConnection {

	private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private final String DB_URL = "jdbc:mysql://localhost:3306/tracking";
	private final String user = "maria";
	private final String pass = "maria";
	private Connection c;
	
	public DBConnection() {
		creareConexiune();
	}
	
	/**
	 * @return un obiect de tip Connection*/
	public Connection getConnection() {
		System.out.println("ok conexiune");
		return c;
		
	}
	
	/**
	 * Metoda care realizeaza conexiunea cu baza de date. */
	public void creareConexiune() {
		
		try {
			//Register JDBC driver
			Class.forName(JDBC_DRIVER);
			
			//Open a connection
			c = DriverManager.getConnection(DB_URL, user, pass);
			System.out.println("ok conexiune");

			//conn.close();
		} catch(SQLException se) {
			System.out.println("nu1");
			se.printStackTrace();
		} catch(Exception e) {
			System.out.println("nu2");
			e.printStackTrace();
		}
	}
}
