package extra;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.tempuri.Package;

public class PackageDAO {

	private DBConnection db = new DBConnection();

	public PackageDAO() {

	}

	public List<Package> getAllPackages() {
		List<Package> packages = new ArrayList<Package>();
		Statement stmt = null;
		try {
			db.getConnection().setAutoCommit(false);
			stmt = db.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("select * from package");
			db.getConnection().commit();
			while (rs.next()) {
				Package input = new Package();
				input.setId(rs.getInt(1));
				input.setIdSender(rs.getInt(2));
				input.setIdReceiver(rs.getInt(3));
				input.setName(rs.getString(4));
				input.setDescription(rs.getString(5));
				input.setSenderCity(rs.getString(6));
				input.setDestinationCity(rs.getString(7));
				input.setTracking(rs.getString(8));
				System.out.println(input.toString());
				packages.add(input);
			}
			stmt.close();
		} catch (SQLException se) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			se.printStackTrace();
		}
		return packages;
	}
}
