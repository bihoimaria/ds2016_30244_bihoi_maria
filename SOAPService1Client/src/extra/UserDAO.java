package extra;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {

	private DBConnection db = new DBConnection();

	public UserDAO() {

	}

	public int getUserId(String name) {
		Statement stmt = null;
		int idUser = 0;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "select iduser from user where name= '" + name + "'";
			stmt = db.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			db.getConnection().commit();
			rs.next();
			idUser = rs.getInt(1);
			stmt.close();
			System.out.println("ok dao cu:" + idUser);
			System.out.println("ok get user");
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail get user");
			e.printStackTrace();
		}
		return idUser;
	}
	
	public String getUserName(int id) {
		Statement stmt = null;
		String name = null;
		try {
			db.getConnection().setAutoCommit(false);
			String sql = "select name from user where iduser= " + id;
			stmt = db.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			db.getConnection().commit();
			rs.next();
			name = rs.getString(1);
			stmt.close();
			System.out.println("ok get user");
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			System.out.println("fail get user");
			e.printStackTrace();
		}
		return name;
	}
}
