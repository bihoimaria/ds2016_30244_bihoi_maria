
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="searchPackageByNameResult" type="{http://tempuri.org/}ArrayOfPackage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchPackageByNameResult"
})
@XmlRootElement(name = "searchPackageByNameResponse")
public class SearchPackageByNameResponse {

    protected ArrayOfPackage searchPackageByNameResult;

    /**
     * Gets the value of the searchPackageByNameResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPackage }
     *     
     */
    public ArrayOfPackage getSearchPackageByNameResult() {
        return searchPackageByNameResult;
    }

    /**
     * Sets the value of the searchPackageByNameResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPackage }
     *     
     */
    public void setSearchPackageByNameResult(ArrayOfPackage value) {
        this.searchPackageByNameResult = value;
    }

}
