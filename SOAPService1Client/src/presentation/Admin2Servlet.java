package presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Admin2Servlet
 */
@WebServlet("/Admin2Servlet")
public class Admin2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Admin2Servlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		request.getRequestDispatcher("link.html").include(request, response);

		HttpSession session = request.getSession();

		if (session.getAttribute("name") == null) {
			response.sendRedirect("login.html");
		} else {
			out.print("<h2>Hello " + session.getAttribute("name") + "!</h2>");
			request.getRequestDispatcher("admin.html").include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		request.getRequestDispatcher("link.html").include(request, response);

		response.setHeader("Cache-Control", "no-cache"); // HTTP/1.1 Proxy
															// Servers
		response.setDateHeader("Expires", 0); // for Browsers

		service3.org.tempuri2.Service1 serv = new service3.org.tempuri2.Service1();
		service3.org.tempuri2.Service1Soap se = serv.getService1Soap();

		String op = request.getParameter("op");
		if (op.equals("create")) {
			request.getRequestDispatcher("create.html").include(request, response);
		} else if (op.equals("register")) {
			service3.org.tempuri2.ArrayOfPackage array = se.getAllPackages();
			List<service3.org.tempuri2.Package> list = array.getPackage();
			out.print("<form action=\"RegisterPServlet\" method=\"post\" >" + "<table border=\"1\">");
			for (int i = 0; i < list.size(); i++) {
				String to = se.getUserName(list.get(i).getIdReceiver());
				String from = se.getUserName(list.get(i).getIdSender());
				out.print("<tr>");
				out.print("<td>" + from + "</td>");
				out.print("<td>" + to + "</td>");
				out.print("<td>" + list.get(i).getName() + "</td>");
				out.print("<td>" + list.get(i).getDescription() + "</td>");
				out.print("<td>" + list.get(i).getSenderCity() + "</td>");
				out.print("<td>" + list.get(i).getDestinationCity() + "</td>");
				out.print("<td>" + list.get(i).getTracking() + "</td>");
				out.print("<th> <input type=\"checkbox\" name=\"checkboxGroup\" value=" + list.get(i).getId()
						+ "/> </th>");
				System.out.println(list.get(i).getId());
				out.print("</tr>");
			}
			out.print("</table><br>" + "Fill the field with the new route<br>"
					+ "City: <input type=\"text\" name = \"city\"><br>"
					+ " <input type=\"submit\" value=\"Register package\"/> </form>");
		} else if (op.equals("update")) {
			service3.org.tempuri2.ArrayOfPackage array = se.getAllPackages();
			List<service3.org.tempuri2.Package> list = array.getPackage();
			out.print("<form action=\"UpdateServlet\" method=\"post\" >" + "<table border=\"1\">");
			for (int i = 0; i < list.size(); i++) {
				String to = se.getUserName(list.get(i).getIdReceiver());
				String from = se.getUserName(list.get(i).getIdSender());
				out.print("<tr>");
				out.print("<td>" + from + "</td>");
				out.print("<td>" + to + "</td>");
				out.print("<td>" + list.get(i).getName() + "</td>");
				out.print("<td>" + list.get(i).getDescription() + "</td>");
				out.print("<td>" + list.get(i).getSenderCity() + "</td>");
				out.print("<td>" + list.get(i).getDestinationCity() + "</td>");
				out.print("<td>" + list.get(i).getTracking() + "</td>");
				out.print("<th> <input type=\"checkbox\" name=\"checkboxGroup\" value=" + list.get(i).getId()
						+ "/> </th>");
				System.out.println(list.get(i).getId());
				out.print("</tr>");
			}
			out.print("</table><br>" 
					+ " <input type=\"submit\" value=\"Get status\"/> </form>");
		} else if (op.equals("delete")) {
			service3.org.tempuri2.ArrayOfPackage array = se.getAllPackages();
			List<service3.org.tempuri2.Package> list = array.getPackage();
			out.print("<form action=\"DeleteServlet\" method=\"post\" >" + "<table border=\"1\">");
			for (int i = 0; i < list.size(); i++) {
				String to = se.getUserName(list.get(i).getIdReceiver());
				String from = se.getUserName(list.get(i).getIdSender());
				out.print("<tr>");
				out.print("<td>" + from + "</td>");
				out.print("<td>" + to + "</td>");
				out.print("<td>" + list.get(i).getName() + "</td>");
				out.print("<td>" + list.get(i).getDescription() + "</td>");
				out.print("<td>" + list.get(i).getSenderCity() + "</td>");
				out.print("<td>" + list.get(i).getDestinationCity() + "</td>");
				out.print("<td>" + list.get(i).getTracking() + "</td>");
				out.print("<th> <input type=\"checkbox\" name=\"checkboxGroup\" value=" + list.get(i).getId()
						+ "/> </th>");
				System.out.println(list.get(i).getId());
				out.print("</tr>");
			}
			out.print("</table><br> <input type=\"submit\" value=\"Delete package\"/> </form>");
		} else {
			response.sendError(404, "The server can not find the requested page.");
		}
	}

}
