package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import business.PackageServiceImpl;
import business.PackageServiceImplServiceLocator;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
		
		String from = request.getParameter("from");
    	String to = request.getParameter("to");
    	String name = request.getParameter("name");
    	String description = request.getParameter("description");
    	String senderCity = request.getParameter("senderCity");
    	String destinationCity = request.getParameter("destinationCity");
    	PackageServiceImplServiceLocator service = new PackageServiceImplServiceLocator();
        try {
			PackageServiceImpl s = service.getPackageServiceImpl();
			boolean ok = s.addPackage(from, to, name, description, senderCity, destinationCity);
	    	if(ok) {
	    		out.println("<script type=\"text/javascript\">");  
	        	out.println("alert('The package was successfully added');");  
	        	out.println("</script>");
	    	} 
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
