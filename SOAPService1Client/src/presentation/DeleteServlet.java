package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import business.PackageServiceImpl;
import business.PackageServiceImplServiceLocator;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
        
        String[] values = request.getParameterValues("checkboxGroup");
        
        PackageServiceImplServiceLocator service = new PackageServiceImplServiceLocator();
        PackageServiceImpl s;
		try {
			s = service.getPackageServiceImpl();
	        for(int i=0; i<values.length; i++) {
	        	String[] id = values[i].split("/");
	        	boolean ok = s.deletePackage(Integer.parseInt(id[0]));
	            if(ok) {
	            	out.println("<script type=\"text/javascript\">");  
	            	out.println("alert('The package with id " + values[i] + " was successfully deleted');");  
	            	out.println("</script>");
	            } else {
	            	response.sendError(500, "The request was not completed. The server met an unexpected condition.");
	            }
	        }
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

}
