package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.tempuri.Service1;
import org.tempuri.Service1Soap;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		request.getRequestDispatcher("link.html").include(request, response);

		response.setHeader("Cache-Control", "no-cache"); // HTTP/1.1 Proxy
															// Servers
		response.setDateHeader("Expires", 0); // for Browsers

		try {
			String name = request.getParameter("name");
			String password = request.getParameter("password");

			Service1 service = new Service1();
			Service1Soap s = service.getService1Soap();

			// s.getUserPackages(name)

			String role = s.logIn(name, password);

			if (role.equals("admin") && htmlFilter(password.trim()).length() != 0
					&& htmlFilter(name.trim()).length() != 0) {
				request.getRequestDispatcher("admin.html").include(request, response);
				HttpSession session = request.getSession();
				session.setAttribute("name", name);
				session.setMaxInactiveInterval(30 * 60);
				response.sendRedirect(request.getContextPath() + "/Admin2Servlet");
			} else if (role.equals("user") && htmlFilter(password.trim()).length() != 0
					&& htmlFilter(name.trim()).length() != 0) {
				request.getRequestDispatcher("user.html").include(request, response);
				HttpSession session = request.getSession();
				session.setAttribute("name", name);
				session.setMaxInactiveInterval(30 * 60);
				response.sendRedirect(request.getContextPath() + "/User2Servlet");
			}
		} catch (NullPointerException e) {
			response.sendError(407, "You must authenticate with a proxy server before this request can be served.");
		}
		out.close();
	}

	private String htmlFilter(String message) {
		if (message == null)
			return null;
		int len = message.length();
		StringBuffer result = new StringBuffer(len + 20);
		char aChar;

		for (int i = 0; i < len; ++i) {
			aChar = message.charAt(i);
			switch (aChar) {
			case '<':
				result.append("&lt;");
				break;
			case '>':
				result.append("&gt;");
				break;
			case '&':
				result.append("&amp;");
				break;
			case '"':
				result.append("&quot;");
				break;
			default:
				result.append(aChar);
			}
		}
		return (result.toString());
	}
}
