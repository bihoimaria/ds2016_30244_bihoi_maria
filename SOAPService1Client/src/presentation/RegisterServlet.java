package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.tempuri.Service1;
import org.tempuri.Service1Soap;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);  
        
        response.setHeader("Cache-Control", "no-cache"); //HTTP/1.1 Proxy Servers
        response.setDateHeader("Expires", 0); // for Browsers
        
        String name=request.getParameter("name");  
        String password=request.getParameter("password");  
        
        Service1 service = new Service1();
        Service1Soap s =  service.getService1Soap();
        
        boolean ok = s.registerUser(name, password);
        if(ok) {
        	out.println("<script type=\"text/javascript\">");  
        	out.println("alert('Contul tau a fost creat cu succes');");  
        	out.println("</script>");
        	HttpSession session=request.getSession();  
            session.invalidate(); 
        }
	}

}
