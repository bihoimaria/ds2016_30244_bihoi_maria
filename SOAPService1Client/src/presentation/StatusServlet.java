package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import business.PackageServiceImpl;
import business.PackageServiceImplServiceLocator;

/**
 * Servlet implementation class StatusServlet
 */
@WebServlet("/StatusServlet")
public class StatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatusServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
        
        HttpSession session = request.getSession();
        
        String city = request.getParameter("city");
        System.out.println(city);
        
        PackageServiceImplServiceLocator service = new PackageServiceImplServiceLocator();
        PackageServiceImpl s;
        try {
			s = service.getPackageServiceImpl();
			if(city!="") {
        		boolean ok = s.updateStatus(city, (int) session.getAttribute("idPackage"));
        		if(ok) {
        			out.println("<script type=\"text/javascript\">");  
                	out.println("alert('A new route was added to package with id " + (int) session.getAttribute("idPackage") + "');");  
                	out.println("</script>");
        		} else {
        			response.sendError(500, "The request was not completed. The server met an unexpected condition.");
        		}
        	}
	} catch (ServiceException e) {
		e.printStackTrace();
	}
	}

}
