package presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.tempuri.ArrayOfRoute;
import org.tempuri.Route;
import org.tempuri.Service1;
import org.tempuri.Service1Soap;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		request.getRequestDispatcher("link.html").include(request, response);

		HttpSession session = request.getSession();
		String[] values = request.getParameterValues("checkboxGroup");

		Service1 service1 = new Service1();
		Service1Soap s1 = service1.getService1Soap();
		for (int i = 0; i < values.length; i++) {
			String[] id = values[i].split("/");
			ArrayOfRoute array = s1.checkStatus(Integer.parseInt(id[0]));
			List<Route> list = array.getRoute();

			session.setAttribute("idPackage", Integer.parseInt(id[0]));

			out.print("<ul>");
			for (int j = 0; j < list.size(); j++) {
				String item = "Route [id = " + list.get(j).getId() + ", city = " + list.get(j).getCity() + ", time = "
						+ list.get(j).getTime();
				out.print("<li>" + item + "</li>");
			}
			out.print("</ul>");

			out.print("<form action=\"StatusServlet\" method=\"post\" >");
			out.print("Fill the field with the new route<br>" + "City: <input type=\"text\" name = \"city\"><br>"
					+ " <input type=\"submit\" value=\"Update status\"/> </form>");
		}
	}
}
