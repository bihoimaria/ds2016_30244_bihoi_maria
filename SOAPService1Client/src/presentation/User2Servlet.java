package presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.tempuri.ArrayOfPackage;
import org.tempuri.ArrayOfRoute;
import org.tempuri.Package;
import org.tempuri.Route;
import org.tempuri.Service1;
import org.tempuri.Service1Soap;

/**
 * Servlet implementation class User2Servlet
 */
@WebServlet("/User2Servlet")
public class User2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//private UserDAO u = new UserDAO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User2Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		request.getRequestDispatcher("link.html").include(request, response);

		HttpSession session = request.getSession();
		
		Service1 service = new Service1();
        Service1Soap s =  service.getService1Soap();
        
        service3.org.tempuri2.Service1 serv = new service3.org.tempuri2.Service1();
        service3.org.tempuri2.Service1Soap se = serv.getService1Soap();
        
		if (session.getAttribute("name") == null) {
			response.sendRedirect("login.html");
		} else {
			out.print("<h2>Hello " + session.getAttribute("name") + "!</h2>");
			out.print("<h3>Your packages are:</h3>");
			ArrayOfPackage array = s.getUserPackages((String) session.getAttribute("name"));
			List<Package> list = array.getPackage();
			
			out.print("<ul>");
            for(int i=0; i<list.size(); i++) {
            	//int idUser = u.getUserId((String) session.getAttribute("name"));
            	int idUser = se.getUserId((String) session.getAttribute("name"));
            	if(idUser == list.get(i).getIdSender()) {
            		//String to = u.getUserName(list.get(i).getIdReceiver());
            		String to = se.getUserName(list.get(i).getIdReceiver());
            		String item = "Package [id = " + list.get(i).getId() + ", from you to = " + to + ", name = " + list.get(i).getName() + 
                			", description = " + list.get(i).getDescription() + ", senderCity = " + list.get(i).getSenderCity() + 
                			", destinationCity = " + list.get(i).getDestinationCity() + "]";
                	out.print("<li>" + item + "</li>");
            	} else if(idUser == list.get(i).getIdReceiver()) {
            		//String to = u.getUserName(list.get(i).getIdSender());
            		String to = se.getUserName(list.get(i).getIdSender());
            		String item = "Package [id = " + list.get(i).getId() + ", to = " + to + " from you" + ", name = " + list.get(i).getName() + 
                			", description = " + list.get(i).getDescription() + ", senderCity = " + list.get(i).getSenderCity() + 
                			", destinationCity = " + list.get(i).getDestinationCity() + "]";
                	out.print("<li>" + item + "</li>");
            	}
            	
            }
            out.print("</ul>");
			
            request.getRequestDispatcher("user.html").include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
		
		String name=request.getParameter("namePackage");
        String id=request.getParameter("idPackage");
        
        Service1 service = new Service1();
        Service1Soap s =  service.getService1Soap();
        
        if(name!="") {
        	ArrayOfPackage array = s.searchPackageByName(name);
        	List<Package> list = array.getPackage();
        	out.print("<ul>");
            for(int i=0; i<list.size(); i++) {
            	String item = "Package [id = " + list.get(i).getId() + ", name = " + list.get(i).getName() + 
            			", description = " + list.get(i).getDescription() + ", senderCity = " + list.get(i).getSenderCity() + 
            			", destinationCity = " + list.get(i).getDestinationCity() + ", tracking = " + list.get(i).getTracking() + "]";
            	out.print("<li>" + item + "</li>");
            }
            out.print("</ul>");
        }
        
        if(id!="") {
        	ArrayOfRoute array = s.checkStatus(Integer.parseInt(id));
        	List<Route> list = array.getRoute();
        	out.print("<ul>");
            for(int i=0; i<list.size(); i++) {
            	String item = "Route [id = " + list.get(i).getId() + ", city = " + list.get(i).getCity() + 
            			", time = " + list.get(i).getTime();
            	out.print("<li>" + item + "</li>");
            }
            out.print("</ul>");
        }
	}

}
