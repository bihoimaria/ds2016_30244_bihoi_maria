﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebService
{
    interface Interface1
    {
        List<Package> getUserPackages(string name);

        List<Package> searchPackageByName(string name);

        List<Route> checkStatus(int id);

        string logIn(string name, string password);

        bool registerUser(string name, string password);
    }
}
