﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService
{
    public class Package
    {
        private object p1;
        private object p2;
        private object p3;
        private object p4;
        private object p5;
        private object p6;
        private object p7;

        public Package() { }
        public Package(int idSender, int idReceiver, string name, string description, string senderCity,
                string destinationCity, string tracking)
        {
            this.idSender = idSender;
            this.idReceiver = idReceiver;
            this.name = name;
            this.description = description;
            this.senderCity = senderCity;
            this.destinationCity = destinationCity;
            this.tracking = tracking;
        }

        public int id { get; set; }

        public int idSender { get; set; }

        public int idReceiver { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public string senderCity { get; set; }

        public string destinationCity { get; set; }

        public string tracking { get; set; }

        public override string ToString()
        {
            return "Package [sender=" + idSender + ", receiver=" + idReceiver + ", name=" + name + ", description="
                    + description + ", senderCity=" + senderCity + ", destinationCity=" + destinationCity + ", tracking="
                    + tracking + "]";
        }
    }
}