﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService
{
    public class PackageDAO
    {
        DBConnection db = new DBConnection();

        public PackageDAO(){ }

        public List<Package> getPackages(int idUser)
        {
            string query = "select * from package where sender_iduser = " + idUser + " or receiver_iduser = " + idUser;
            List<Package> packages = new List<Package>();
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();
                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Package input = new Package(Convert.ToInt32(dataReader["sender_iduser"]), Convert.ToInt32(dataReader["receiver_iduser"]),
                        Convert.ToString(dataReader["name"]), Convert.ToString(dataReader["description"]), 
                        Convert.ToString(dataReader["senderCity"]), Convert.ToString(dataReader["destinationCity"]), 
                        Convert.ToString(dataReader["tracking"]));
                    input.id = Convert.ToInt32(dataReader["idpackage"]);
                    Console.WriteLine(input.ToString());
                    packages.Add(input);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return packages;
            }
            else
            {
                return packages;
            }
        }

        public List<Package> getPackageByName(string name)
        {
            string query = "select * from package where name = '" + name + "'";
            List<Package> packages = new List<Package>();
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();
                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Package input = new Package(Convert.ToInt32(dataReader["sender_iduser"]), Convert.ToInt32(dataReader["receiver_iduser"]),
                        Convert.ToString(dataReader["name"]), Convert.ToString(dataReader["description"]),
                        Convert.ToString(dataReader["senderCity"]), Convert.ToString(dataReader["destinationCity"]),
                        Convert.ToString(dataReader["tracking"]));
                    input.id = Convert.ToInt32(dataReader["idpackage"]);
                    Console.WriteLine(input.ToString());
                    packages.Add(input);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return packages;
            }
            else
            {
                return packages;
            }
        }

        public string checkStatus(int id)
        {
            string query = "select tracking from package where idpackage = " + id;
            string status="";
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                status = (string)cmd.ExecuteScalar();
                
                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return status;
            }
            else
            {
                return status;
            }
        }
    }
}