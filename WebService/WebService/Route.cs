﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService
{
    public class Route
    {
        public Route() { }
        public Route(string city, DateTime time, int idPackage)
        {
            this.city = city;
            this.time = time;
            this.idPackage = idPackage;
        }

        public int id { get; set; }

        public string city { get; set; }

        public DateTime time { get; set; }

        public int idPackage { get; set; }

        public override string ToString()
        {
            return "Route [id=" + id + ", city=" + city + ", time=" + time + ", idPackage=" + idPackage + "]";
        }
    }
}