﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService
{
    public class RouteDAO
    {
        DBConnection db = new DBConnection();

        public RouteDAO(){ }

        public List<Route> checkStatus(int idPackage)
        {
            string query = "select * from route where package_idpackage = " + idPackage;
            List<Route> routes = new List<Route>();
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();
                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Route input = new Route(Convert.ToString(dataReader["city"]), Convert.ToDateTime(dataReader["time"]),
                        Convert.ToInt32(dataReader["package_idpackage"]));
                    input.id = Convert.ToInt32(dataReader["idroute"]);
                    Console.WriteLine(input.ToString());
                    routes.Add(input);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return routes;
            }
            else
            {
                return routes;
            }
        }
    }
}