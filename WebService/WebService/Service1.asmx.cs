﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService, Interface1
    {
        UserDAO u = new UserDAO();
	    PackageDAO p = new PackageDAO();
	    RouteDAO r = new RouteDAO();

        [WebMethod]
        public List<Package> getUserPackages(string name)
        {
            int idUser = u.getUser(name);
            return p.getPackages(idUser);
        }

        [WebMethod]
        public List<Package> searchPackageByName(string name)
        {
            return p.getPackageByName(name);
        }

         [WebMethod]
         public List<Route> checkStatus(int id)
         {
             return r.checkStatus(id);
         }

         [WebMethod]
         public string logIn(string name, string password)
         {
             return u.getUserRole(name, password);
         }

         [WebMethod]
         public bool registerUser(string name, string password)
         {
             return u.registerUser(name, password, "user");
         }
    }
}