﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService
{
    public class UserDAO
    {
        DBConnection db = new DBConnection();

        public UserDAO(){ }

        public bool registerUser(string name, string pass, string role)
        {
            string query = "insert into user (name, password, role) values (" + "'" + name + "'" + ", " + "'" + pass + "'"
                    + ", " + "'" + role + "')";
            bool ok = false;

            //open connection
            if (db.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, db.connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                db.CloseConnection();

                ok = true;
            }
            return ok;
        }

        public String getUserRole(string name, string password)
        {
            string query = "select role from user where name= '" + name + "' and password= '" + password + "'";
            string role = "";
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                role = (string)cmd.ExecuteScalar();

                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return role;
            }
            else
            {
                return role;
            }
        }

        public int getUser(string name)
        {
            string query = "select iduser from user where name= '" + name + "'";
            int id = 0;
            if (db.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                id = (int)cmd.ExecuteScalar();

                //close Connection
                db.CloseConnection();
                //return list to be displayed
                return id;
            }
            else
            {
                return id;
            }
        }
    }
}