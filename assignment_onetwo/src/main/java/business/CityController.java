package business;

import org.hibernate.cfg.Configuration;

import dao.CityDAO;
import entities.City;

public class CityController {
	
	private CityDAO c = new CityDAO(new Configuration().configure().buildSessionFactory());

	public CityController(){}
	
	public City findCity(String name) {
		return c.findCity(name);
	}
	
	public String getLatitude(String nameCity) {
		return c.findCity(nameCity).getLatitude();
	}
	
	public String getLongitude(String nameCity) {
		return c.findCity(nameCity).getLongitude();
	}
}
