package business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.hibernate.cfg.Configuration;

import dao.FlightDAO;
import entities.Flight;

public class FlightController {
	
	private FlightDAO f = new FlightDAO(new Configuration().configure().buildSessionFactory());
	private CityController c = new CityController();

	public FlightController() {}
	
	public List<Flight> getAllFlights() {
		return f.getFlights();
	}
	
	@SuppressWarnings("unchecked")
	public int addFlight(String type, String departureCity, String departureDate, String arrivalCity, String arrivalDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date, date2;
		int id = 0;
		try {
			date = (Date) sdf.parse(departureDate);
			date2 = (Date) sdf.parse(arrivalDate);
			@SuppressWarnings("rawtypes")
			HashSet cities = new HashSet();
			cities.add(c.findCity(departureCity));
			cities.add(c.findCity(arrivalCity));
			id = f.addFlights(type, departureCity, date, arrivalCity, date2, cities);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public boolean deleteFlight(String id) {
		String[] idString = id.split("/");
		int flightId = Integer.parseInt(idString[0]);
		return f.deleteFlight(flightId);
	}
	
	public boolean updateArrival(String id, String hour) {
		String[] idString = id.split("/");
		int flightId = Integer.parseInt(idString[0]);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		Date date;
		boolean ok = false;
		try {
			date = (Date) sdf.parse(hour);
			ok = f.updateArrivalDate(flightId, date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ok;
	}
	
	public boolean updateDeparture(String id, String hour) {
		int flightId = Integer.parseInt(id.substring(0, 1));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		Date date;
		boolean ok = false;
		try {
			date = (Date) sdf.parse(hour);
			ok = f.updateDepartureDate(flightId, date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ok;
	}
	
	public Date getDepartureDate(String city) {
		return f.getFlightDeparture(city).getDepartureDate();
	}
	
	public Date getArrivalDate(String city) {
		return f.getFlightArrival(city).getArrivalDate();
	}
}
