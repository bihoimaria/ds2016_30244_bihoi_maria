package business;

import org.hibernate.cfg.Configuration;

import dao.UserDAO;
import entities.User;

public class UserController {
	
	private UserDAO u = new UserDAO(new Configuration().configure().buildSessionFactory());
	
	public UserController() {}
	
	public String logIn(String name, String pass) {
		User user = u.findUser(name, pass);
		return user.getRole();
	}

}
