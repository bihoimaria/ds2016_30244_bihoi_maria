package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.City;

public class CityDAO {

	private SessionFactory factory;

	public CityDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	public void addCity(String name, String latitude, String longitude) {
		Session session = factory.openSession();
	      Transaction tx = null;
	      @SuppressWarnings("unused")
		Integer cityId = null;
	      try{
	         tx = session.beginTransaction();
	         City city = new City(name, latitude, longitude);
	         cityId = (Integer) session.save(city); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	}
	
	@SuppressWarnings("unchecked")
	public City findCity(String name) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE name = :name");
			query.setParameter("name", name);
			cities = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0) : null;
	}
}
