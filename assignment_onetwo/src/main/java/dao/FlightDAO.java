package dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.*;

import entities.Flight;

public class FlightDAO {

	private SessionFactory factory;

	public FlightDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	@SuppressWarnings("rawtypes")
	public Integer addFlights(String type, String departureCity, Date departureDate, String arrivalCity, Date arrivalDate, Set cities) {
		Session session = factory.openSession();
	      Transaction tx = null;
	      Integer flightId = null;
	      try{
	         tx = session.beginTransaction();
	         Flight flight = new Flight(type, departureCity, departureDate, arrivalCity, arrivalDate);
	         flight.setCities(cities);
	         flightId = (Integer) session.save(flight); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	    return flightId;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> getFlights() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights;
	}
	
	public boolean updateArrivalDate(Integer flightId, Date arrivalDate ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      boolean ok = false;
	      try{
	         tx = session.beginTransaction();
	         Flight flight = (Flight)session.get(Flight.class, flightId); 
	         flight.setArrivalDate(arrivalDate);
			 session.update(flight); 
	         tx.commit();
	         ok = true;
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return ok;
	   }
	
	public boolean updateDepartureDate(Integer flightId, Date departureDate ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      boolean ok = false;
	      try{
	         tx = session.beginTransaction();
	         Flight flight = (Flight)session.get(Flight.class, flightId); 
	         flight.setArrivalDate(departureDate);
			 session.update(flight); 
	         tx.commit();
	         ok = true;
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return ok;
	   }
	
	public boolean deleteFlight(Integer flightId){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      boolean ok=false;
	      try{
	         tx = session.beginTransaction();
	         Flight flight = (Flight)session.get(Flight.class, flightId); 
	         session.delete(flight); 
	         ok=true;
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return ok;
	   }
	
	@SuppressWarnings("unchecked")
	public Flight getFlightDeparture(String departureCity) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE departurecity = :departureCity");
			query.setParameter("departureCity", departureCity);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public Flight getFlightArrival(String arrivalCity) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE arrivalcity = :arrivalCity");
			query.setParameter("arrivalCity", arrivalCity);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
}
