package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.User;

public class UserDAO {
	
	private SessionFactory factory;

	public UserDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	public Integer addUser(String name, String pass, String role) {
		Session session = factory.openSession();
	      Transaction tx = null;
	      Integer userId = null;
	      try{
	         tx = session.beginTransaction();
	         User user = new User(name, pass, role);
	         userId = (Integer) session.save(user); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return userId;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			users = session.createQuery("FROM User").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	public User findUser(String name, String password) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE name = :name AND password = :password");
			query.setParameter("name", name);
			query.setParameter("password", password);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}
}
