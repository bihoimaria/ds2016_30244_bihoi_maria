package entities;

import java.util.Date;
import java.util.Set;

public class Flight {

	private int id;
	private String type;
	private String departureCity;
	private Date departureDate;
	private String arrivalCity;
	private Date arrivalDate;
	@SuppressWarnings("rawtypes")
	private Set cities;
	
	public Flight() {}
	public Flight(String type, String departureCity, Date departureDate, String arrivalCity, Date arrivalDate) {
		this.type = type;
		this.departureCity = departureCity;
		this.departureDate = departureDate;
		this.arrivalCity = arrivalCity;
		this.arrivalDate = arrivalDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	@SuppressWarnings("rawtypes")
	public Set getCities() {
		return cities;
	}
	@SuppressWarnings("rawtypes")
	public void setCities(Set cities) {
		this.cities = cities;
	}
	@Override
	public String toString() {
		return "Type: " + type + ", DepartureCity: " + departureCity + ", DepartureDate: " + departureDate
				+ ", ArrivalCity: " + arrivalCity + ", ArrivalDate: " + arrivalDate;
	}
}
