package presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import business.FlightController;
import entities.Flight;

/**
 * Servlet implementation class AdminServlet
 */
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightController f = new FlightController();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
        
        response.setHeader("Cache-Control", "no-cache"); //HTTP/1.1 Proxy Servers
        response.setDateHeader("Expires", 0); // for Browsers
        
        HttpSession session = request.getSession();
        
        if(session.getAttribute("name") == null){
        	response.sendRedirect("login.html");
        } else {
	        out.print("<h2>Hello " + session.getAttribute("name") + "!</h2>");
	        request.getRequestDispatcher("admin.html").include(request, response);
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
        
        response.setHeader("Cache-Control", "no-cache"); //HTTP/1.1 Proxy Servers
        response.setDateHeader("Expires", 0); // for Browsers
        
        String op = request.getParameter("op");
        if(op.equals("create")) {
        	request.getRequestDispatcher("create.html").include(request, response);
        } else if(op.equals("read")) {
        	List<Flight> list = f.getAllFlights();
            out.print("<ul>");
            for(int i=0; i<list.size(); i++) {
            	out.print("<li>" + list.get(i).toString() + "</li>");
            }
            out.print("</ul>");
        } else if(op.equals("update")) {
        	List<Flight> list = f.getAllFlights();
        	out.print("<form action=\"UpdateServlet\" method=\"post\" >" + "<table border=\"1\">");
        	for(int i=0; i<list.size(); i++) {
        		out.print("<tr>");
            	out.print("<td>" + list.get(i).getType() + "</td>");
            	out.print("<td>" + list.get(i).getDepartureCity() + "</td>");
            	out.print("<td>" + list.get(i).getDepartureDate() + "</td>");
            	out.print("<td>" + list.get(i).getArrivalCity() + "</td>");
            	out.print("<td>" + list.get(i).getArrivalDate() + "</td>");
            	out.print("<th> <input type=\"checkbox\" name=\"checkboxGroup\" value=" + list.get(i).getId() + "/> </th>");
            	System.out.println(list.get(i).getId());
            	out.print("</tr>");
            }
        	out.print("</table><br>"
        			+ "Ora noua plecare: <input type=\"text\" name = \"plecnou\"><br>"
        			+ "Ora noua sosire: <input type=\"text\" name = \"sosnou\"><br>"
        			+ " <input type=\"submit\" value=\"Update\"/> </form>");
        } else if(op.equals("delete")) {
        	List<Flight> list = f.getAllFlights();
        	out.print("<form action=\"DeleteServlet\" method=\"post\" >" + "<table border=\"1\">");
        	for(int i=0; i<list.size(); i++) {
        		out.print("<tr>");
            	out.print("<td>" + list.get(i).getType() + "</td>");
            	out.print("<td>" + list.get(i).getDepartureCity() + "</td>");
            	out.print("<td>" + list.get(i).getDepartureDate() + "</td>");
            	out.print("<td>" + list.get(i).getArrivalCity() + "</td>");
            	out.print("<td>" + list.get(i).getArrivalDate() + "</td>");
            	out.print("<th> <input type=\"checkbox\" name=\"checkboxGroup\" value=" + list.get(i).getId() + "/> </th>");
            	out.print("</tr>");
            }
        	out.print("</table><br> <input type=\"submit\" value=\"Delete\"/> </form>");
        } else {
        	response.sendError(404, "The server can not find the requested page.");
        }
	}

}
