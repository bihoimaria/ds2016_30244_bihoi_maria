package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import business.FlightController;

/**
 * Servlet implementation class CreateServlet
 */
public class CreateServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private FlightController f = new FlightController();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
		
		String type = request.getParameter("type");
    	String departureCity = request.getParameter("plecare");
    	String departureDate = request.getParameter("datap");
    	String arrivalCity = request.getParameter("sosire");
    	String arrivalDate = request.getParameter("datas");
    	int ok = f.addFlight(type, departureCity, departureDate, arrivalCity, arrivalDate);
    	if(ok != 0) {
    		out.println("<script type=\"text/javascript\">");  
        	out.println("alert('Zbor adaugat cu succes');");  
        	out.println("</script>");
    	} 
	}
}
