package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import business.UserController;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserController u = new UserController();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 	response.setContentType("text/html");  
	        PrintWriter out=response.getWriter();  
	        request.getRequestDispatcher("link.html").include(request, response);  
	        
	        response.setHeader("Cache-Control", "no-cache"); //HTTP/1.1 Proxy Servers
	        response.setDateHeader("Expires", 0); // for Browsers
	          
	        try {
	        	String name=request.getParameter("name");  
		        String password=request.getParameter("password");  
		         
		        String role = u.logIn(name, password);
		        
	        	if(role.equals("admin") && htmlFilter(password.trim()).length() != 0 && htmlFilter(name.trim()).length() !=0) {
		        	request.getRequestDispatcher("admin.html").include(request, response);
		        	HttpSession session=request.getSession();
		        	session.setAttribute("name",name);
		        	session.setMaxInactiveInterval(30*60);
		        	response.sendRedirect(request.getContextPath() + "/AdminServlet");
		        } else if(role.equals("user") && htmlFilter(password.trim()).length() != 0 && htmlFilter(name.trim()).length() !=0){
		        	request.getRequestDispatcher("user.html").include(request, response);
		        	HttpSession session=request.getSession();
		        	session.setAttribute("name",name);
		        	session.setMaxInactiveInterval(30*60);
		        	response.sendRedirect(request.getContextPath() + "/UserServlet");
		        } 
	        } catch(NullPointerException e) {
	        	response.sendError(407, "You must authenticate with a proxy server before this request can be served.");
	        	//out.print("Sorry, username or password error!");  
	            //request.getRequestDispatcher("login.html").include(request, response);
	        }
	        out.close();  
	}

	private String htmlFilter(String message) {
	      if (message == null) return null;
	      int len = message.length();
	      StringBuffer result = new StringBuffer(len + 20);
	      char aChar;
	 
	      for (int i = 0; i < len; ++i) {
	         aChar = message.charAt(i);
	         switch (aChar) {
	             case '<': result.append("&lt;"); break;
	             case '>': result.append("&gt;"); break;
	             case '&': result.append("&amp;"); break;
	             case '"': result.append("&quot;"); break;
	             default: result.append(aChar);
	         }
	      }
	      return (result.toString());
	   }
}
