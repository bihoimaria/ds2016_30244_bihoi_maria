package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import business.FlightController;

/**
 * Servlet implementation class UpdateServlet
 */
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightController f = new FlightController();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
        
        String plecare = request.getParameter("plecnou");
        String sosire = request.getParameter("sosnou");
        System.out.println(sosire);
        String[] values = request.getParameterValues("checkboxGroup");
        for(int i=0; i<values.length; i++) {
        	if(plecare!="") {
        		boolean ok = f.updateDeparture(values[i], plecare);
        		if(ok) {
        			out.println("<script type=\"text/javascript\">");  
                	out.println("alert('Ora de plecare a zborului " + values[i] + " a fost updatata cu succes');");  
                	out.println("</script>");
        		} else {
        			response.sendError(500, "The request was not completed. The server met an unexpected condition.");
        		}
        	}
        	if(sosire!="") {
        		boolean ok = f.updateArrival(values[i], sosire);
        		if(ok) {
        			out.println("<script type=\"text/javascript\">");  
                	out.println("alert('Ora de sosire a zborului " + values[i] + " a fost updatata cu succes');");  
                	out.println("</script>");
        		} else {
        			response.sendError(500, "The request was not completed. The server met an unexpected condition.");
        		}
        	}
        }
	}

}
