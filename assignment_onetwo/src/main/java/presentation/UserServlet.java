package presentation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import business.CityController;
import business.FlightController;
import entities.Flight;

/**
 * Servlet implementation class UserServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private FlightController f = new FlightController();
	private CityController c = new CityController();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
        
        HttpSession session = request.getSession();
        
        if(session.getAttribute("name") == null){
        	response.sendRedirect("login.html");
        } else {
        	out.print("<h2>Hello " + session.getAttribute("name") + "!</h2>");
            out.print("<h3>Flights</h3>");
            
            List<Flight> list = f.getAllFlights();
            
            out.print("<ul>");
            for(int i=0; i<list.size(); i++) {
            	out.print("<li>" + list.get(i).toString() + "</li>");
            }
            out.print("</ul>");
            
            request.getRequestDispatcher("user.html").include(request, response);
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        request.getRequestDispatcher("link.html").include(request, response);
		
		String plecare=request.getParameter("plecare");  
		System.out.println(plecare);
        String sosire=request.getParameter("sosire");
        System.out.println(sosire);
        
        if(plecare!="") {
        	String lat = c.getLatitude(plecare);
        	String longit = c.getLongitude(plecare);
        	String urlString = "http://www.new.earthtools.org/timezone-1.1/" + lat + "/" + longit;
        	String responseString = null;
        	URL url = new URL(urlString);
        	HttpURLConnection c = (HttpURLConnection)url.openConnection();  //connecting to url
        	c.setRequestMethod("GET");
        	BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));  //stream to resource
        	String str;
        	while ((str = in.readLine()) != null)   //reading data
        	   responseString += str+"\n";//process the response and save it in some string or so
        	in.close();  //closing stream
        	String[] intermediar = responseString.split("<localtime>");
        	String[] finalTime = intermediar[1].split("</localtime>");
        	out.println("<p>Ora locala plecare: " + finalTime[0] + "</p>");
        }
        
        if(sosire!="") {
        	  System.out.println("ok2");
        	String lat = c.getLatitude(sosire);
        	String longit = c.getLongitude(sosire);
        	String urlString = "http://www.new.earthtools.org/timezone-1.1/" + lat + "/" + longit;
        	String responseString = null;
        	System.out.println("http://www.new.earthtools.org/timezone-1.1/" + lat + "/" + longit);
        	URL url = new URL(urlString);
        	HttpURLConnection c = (HttpURLConnection)url.openConnection();  //connecting to url
        	c.setRequestMethod("GET");
        	BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));  //stream to resource
        	String str;
        	while ((str = in.readLine()) != null)   //reading data
        	   responseString += str+"\n";//process the response and save it in some string or so
        	in.close();  //closing stream
        	String[] intermediar = responseString.split("<localtime>");
        	String[] finalTime = intermediar[1].split("</localtime>");
        	out.println("<p>Ora locala sosire: " + finalTime[0] + "</p>");
        }
	}

}
