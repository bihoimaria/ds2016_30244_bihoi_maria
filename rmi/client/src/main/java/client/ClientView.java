package client;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ClientView extends JFrame{

		private static final long serialVersionUID = 1L;
		private JPanel contentPane;
		private JTextField textCaryear;
		private JTextField textCarengine;
		private JTextField textCarprice;
		private JButton btnTax;
		private JButton btnSelling;
		private JTextArea textArea;

		public ClientView() {
			setTitle("RMI Protocol simulator");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(300, 100, 550, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);

			JLabel lblInsertYear = new JLabel("Insert the year");
			lblInsertYear.setBounds(10, 11, 120, 14);
			contentPane.add(lblInsertYear);

			JLabel lblInsertEngine = new JLabel("Insert the engine capacity");
			lblInsertEngine.setBounds(10, 36, 160, 14);
			contentPane.add(lblInsertEngine);

			JLabel lblInsertPrice = new JLabel("Insert the purchasing price");
			lblInsertPrice.setBounds(10, 61, 160, 14);
			contentPane.add(lblInsertPrice);

			textCaryear = new JTextField();
			textCaryear.setBounds(170, 11, 86, 20);
			contentPane.add(textCaryear);
			textCaryear.setColumns(10);

			textCarengine = new JTextField();
			textCarengine.setBounds(170, 36, 86, 20);
			contentPane.add(textCarengine);
			textCarengine.setColumns(10);

			textCarprice = new JTextField();
			textCarprice.setBounds(170, 61, 86, 20);
			contentPane.add(textCarprice);
			textCarprice.setColumns(10);

			btnTax= new JButton("Compute Tax");
			btnTax.setBounds(290, 20, 130, 23);
			contentPane.add(btnTax);

			btnSelling = new JButton("Compute Selling Price");
			btnSelling.setBounds(290, 50, 160, 23);
			contentPane.add(btnSelling);

			textArea = new JTextArea();
			textArea.setBounds(100, 131, 300, 120);
			contentPane.add(textArea);
		}

		public void addBtnTaxActionListener(ActionListener e) {
			btnTax.addActionListener(e);
		}

		public void addBtnSellingActionListener(ActionListener e) {
			btnSelling.addActionListener(e);
		}

		public int getCarYear() {
			return Integer.parseInt(textCaryear.getText());
		}

		public int getCarEngine() {
			return Integer.parseInt(textCarengine.getText());
		}

		public double getCarPrice() {
			return Double.parseDouble(textCarprice.getText());
		}

		public void printResult(String result) {
			textArea.setText(result);
		}
	}