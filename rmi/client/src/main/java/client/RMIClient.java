package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;

import common.Car;
import common.IServices;

public class RMIClient {
	
	private ClientView view;
	
	public RMIClient() {
		view = new ClientView();
        view.setVisible(true);
        view.addBtnTaxActionListener(new ComputeTax());
        view.addBtnSellingActionListener(new ComputeSelling());
	}

	public class ComputeTax implements ActionListener {

		public void actionPerformed(ActionEvent arg0){
			try {
				Registry registry = LocateRegistry.getRegistry("localhost", 1079);
				IServices taxService = (IServices) registry.lookup("server.Service");
				if(view.getCarYear() < 1) {
					JOptionPane.showMessageDialog(null, "Anul masinii trebuie sa fie un numar pozitiv, nenul!");
				} else if(view.getCarEngine() < 1) {
					JOptionPane.showMessageDialog(null, "Capacitatea motorului masinii trebuie sa fie un numar pozitiv, nenul!");
				} else if(view.getCarPrice() < 0) {
					JOptionPane.showMessageDialog(null, "Pretul masinii trebuie sa fie un numar pozitiv!");
				} else {
					Car car = new Car(view.getCarYear(), view.getCarEngine(), view.getCarPrice());
					double result = taxService.computeTax(car);
					view.printResult(Double.toString(result));
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NotBoundException e) {
				e.printStackTrace();
			} 
		}
	}
	
	public class ComputeSelling implements ActionListener {
		public void actionPerformed(ActionEvent arg0){
			try {
				Registry registry = LocateRegistry.getRegistry("localhost", 1079);
				IServices sellingService = (IServices) registry.lookup("server.Service");
				if(view.getCarYear() < 1) {
					JOptionPane.showMessageDialog(null, "Anul masinii trebuie sa fie un numar pozitiv, nenul!");
				} else if(view.getCarEngine() < 1) {
					JOptionPane.showMessageDialog(null, "Capacitatea motorului masinii trebuie sa fie un numar pozitiv, nenul!");
				} else if(view.getCarPrice() < 0) {
					JOptionPane.showMessageDialog(null, "Pretul masinii trebuie sa fie un numar pozitiv!");
				} else {
					Car car = new Car(view.getCarYear(), view.getCarEngine(), view.getCarPrice());
					double result = sellingService.computeSellingPrice(car);
					view.printResult(Double.toString(result));
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException {
		@SuppressWarnings("unused")
		RMIClient client = new RMIClient();
	}
}