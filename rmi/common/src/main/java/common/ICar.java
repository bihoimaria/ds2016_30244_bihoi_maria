package common;

public interface ICar {

	public int getYear();

	public int getEngineCapacity();

	public double getPrice();
}
