package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServices  extends Remote {

	/**
	 * Computes the tax to be payed for a Car.
	 *
	 * @param c Car for which to compute the tax
	 * @return tax for the car
	 */
	public double computeTax(ICar c) throws RemoteException;
	
	/**
	 * Computes the selling price to be payed for a Car.
	 * 
	 * @param c Car for which to compute the selling price
	 * @return selling price
	 */
	public double computeSellingPrice(ICar c) throws RemoteException;
}
