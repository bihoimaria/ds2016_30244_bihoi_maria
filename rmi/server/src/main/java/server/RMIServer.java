package server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import common.IServices;

public class RMIServer {

	public static void main(String[] args) throws RemoteException, MalformedURLException{
		try {
			System.setProperty("java.security.policy","src/main/java/server/server.policy");
			System.setProperty("java.rmi.server.codebase", "file://Z:/facultate/AN%20IV/semI/SD/laborator/rmi/common/target/rmi-common-0.0.1-SNAPSHOT.jar");
			System.setSecurityManager(new SecurityManager());
			
			Registry registry = LocateRegistry.createRegistry(1079);
			registry.rebind("server.Service", new Services());
	        System.out.println("server.RMI Server is ready.");
		} catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
		}
	}
}