package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import common.ICar;
import common.IServices;

public class Services  extends UnicastRemoteObject implements IServices{

	private static final long serialVersionUID = -6499708527784511918L;

	public Services() throws RemoteException {}

	public double computeSellingPrice(ICar c) throws RemoteException {
		return c.getPrice() - (c.getPrice()/7) * (2016 - c.getYear());
	}
	
	public double computeTax(ICar c) throws RemoteException {
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}
}
